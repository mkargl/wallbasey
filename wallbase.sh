#!/bin/bash
#
# A great script for easily downloading wallbase wallpapers from
# http://www.wallbase.cc.
#   Copyright (C) 2012  Michael Kargl <michaelkargl@outlook.com>
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>
#
# ---------------------------------------------------------------------
#
# It is possible to easily configure the script to get only the 
# wallpapers you want. The different sections to configre are documented
# and described below further down in the code.
#
# Also great thanks to MacEarl as he provided the initial base script.
# https://github.com/sevensins/Wallbase-Downloader/blob/master/wallbase.sh
#
export PATH="/usr/local/bin:/usr/bin"



###### -------------- CONFIG ----------------
###### --------------------------------------
#Category : Specifies which wallpapers to look for
# Each being toggled by a 1/0 value
# Combine them as you see fit
    SFW=0
    SCETCHY=1
    NSFW=1


# Random           = 1   Toplist       = 2
# Newest           = 3   Search        = 4
# User collections = 6   Favourites    = 5
# Uploads from User X   = 7
    TYPE=1
    
    
# This Option is used for downloading your Favorites (You need an account for the same)
# and to download Collections created by other users or
# all Wallpapers uploaded from a Specific User
#
# Set the value to "-1" to download your Favorites in your "Home" Collection.
#
# To download User Collections or different Favorite Collections open the desired
# Collection in your Browser and copy the following part
#  1. For your Favorites: http://wallbase.cc/user/favorites/"#number_of_the_collection"
#  2. For user created collections: http://wallbase.cc/user/collection/"#number_of_the_collection"
# You only need the number which is shown at the end of the URL
#
# To download Wallpapers uploaded by a specific user open the profile
# in your Browser and copy the following part
# http://wallbase.cc/user/profile/"#UserID"/uploads
# You only need the number between profile and uploads
    COLLECTION=-1



# Define your Search Query like this:
#  ./wallbase.sh 'A' OR 'B' OR 'C' OR 'D'
#
# If you use AND/OR you need to encapsulate the
# topic in ''s
#
#  For Example set  QUERY="'Japan' OR 'Korea' OR 'China' OR 'Buddha'"
#  Accepted Operators are "AND" and "OR"
    QUERY="'Japan' OR 'Asia' OR 'Shouji' OR 'Kyoto' OR Kanji OR Bamboo"



# Topic : Anime/Manga, Wallpapers/General, High Resolution Images
#   1:  Anime/Manga
#   2:  Wallpapers/General
#   3:  HighResolution Images
# Combine them as you like 123, 12, 23, 3
    TOPIC=123


# Resolution
#   Accepted values are 0 => All Standard
#       800x600 | 1024x768 | 1280x960
#       1280x1024 | 1400x1050 | 1600x1200 | 2560x2048
#   Widescreen
#       1024x600 | 1280x800 | 1366x768 | 1440x900
#       1600x900 | 1680x1050 | 1920x1080 | 1920x1200
#       2560x1440 | 2560x1600
    RESOLUTION=0


# Aspect Ratio
#   Accepted values are 0 => All
#   1.33 => 4:3          1.25 => 5:4
#   1.77 => 16:9         1.60 => 16:10
#   1.70 => Netbook      2.50 => Dual
#   3.20 => Dual Wide    0.99 => Portrait
    ASPECTRATIO=1.77


# Size: at least and Exactly width x height
#   To get at least desired Resolution use gteq
#   To get exactly desired Resolution use eqeq
    SIZE=gteq


# Location all the images should get downloaded into
    LOCATION="./wallbase_wallpapers"


# Best of time [Type 2]:
#  All time = 0     3Months  = 3m
#  2Months  = 2m    1Month   = 1m
#  2Weeks   = 2w    1Week    = 1w
#  3Days    = 3d    1Day     = 1d
    TIME=1d


# Date                 = date
# Amount of Views       = views
# Number of Favorites   = favs
# Relevancie            = relevance
    ORDER=relevance

#  Ascending  = asc
#  Descending = desc
    ORDER_TYPE=desc


# Due to changes in the wallbase.cc "Policy"
#  you now need to login to see NSFW Content
#  or newest Wallpapers
#  It is also needed if you want to download "your own Favorites"
#  or user collections
    USER="user"
    PASS="pass"


# This Option will help you keep your downloaded wallpapers ordered
# It will create subfolders for each TOPIC and CATEGORY
# so you wont have to spend much time looking for a special wallpaper
#
# Set this option to a value greater 0 to set it active
#
# For example:
#  TOPIC="2"
#  SFW=1; SCETCHY=1; NSFW=1;
#  LOCATION="./wallpapers"
#
#  Then the path the folder are being downloaded to looks like this:
#  ./wallpapers/sfw_sketchy_nsfw/2/
    CATEGORIZE=1
    SEPARATE_CATEGORIES=1

    SFW_FOLDERNAME="sfw"
    SCETCHY_FOLDERNAME="scetchy"
    NSFW_FOLDERNAME="nsfw"



# This variable combines with Section 17
# If turned on, the script will only download only one wallpaper from
# wallbase and save it to a specific file ($WALLPAPER_FILE) on your
# system. If turned off it keeps downloading until you stop it or it
# can not find any wallpapers.
#
# If RECYCLE_WALLPAPER is set, already downloaded wallpapers will
# be set as wallpapers as well ( so you dont have to continue to
# search for new ones every time) - This option is best with type RANDOM or NEWEST
    AUTOSET=1
    WALLPAPER_FILE="./wallbase_wallpaper.jpg"
    RECYCLE_WALLPAPER=0
 
 
 # Define the maximum number of wallpapers that you would like to download.
 # maximum value is MAX_RANGE=26460
 # Combines with THPP variable. MAX_RANGE multiplies with THPP.
 # So MAX_RANGE=2 and THPP=20 would download maximum of 40 wallpapers.
    MAX_RANGE=26460

# How many wallpapers are shown on one wallbase-page
# Accepted values are 20, 32, 40, 60
    THPP=20


# Please do not touch or modify these or else you might break something
# Thumbnails per page.

    # This Option will download a specified Range of Wallpapers
    # Set WP_RANGE_STOP to a value greater 0 to set it active
    #
    # For example:
    #  WP_RANGE_START=10000
    #  WP_RANGE_STOP=10200
    #
    #  This Setting will download all Wallpapers from
    #  10.000 to 10.200
    WP_RANGE_START=0
    WP_RANGE_STOP=0

    # get absolute path of LOCATION
    LOCATION="$(readlink -f $LOCATION)"

    # Internal variable that gets set by setWallpaper to notice
    # when a wp got downloaded
    WALLPAPER_SET=0

    # contains all downloaded wallpapernumbers
    WALLPAPER_LOGFILE="$LOCATION/.downloaded_wallpapers.txt"

    PROJECT_DIR="$(readlink -f .)"

    LOGFILE="$PROJECT_DIR/.wallbaselog"


###### ---------- FUNCTIONS ------------------
###### ---------------------------------------


    # Fetches the category depending on user input
    # If categories should be seperated - one of the chosen
    # categories will be chosen randomly. If not the category
    # string will be built up for processing
    #
    # Arguments:
    #   $1 bool: sfw
    #   $2 bool: scetchy
    #   $3 bool: nsfw
    #   $4 bool: seperate categories
    #
    # Example:
    #   fetchCategory 1 1 1  -> one of these outputs "100 010 001"
    #   fetchCategory 1 0 1  -> one of these outputs "100 001"
    #   fetchCategory 1 1 1  -> "111"
    #
    function fetchCategory {
        local sfw=$1
        local scetchy=$2
        local nsfw=$3
        local category="$sfw$scetchy$nsfw"

        # extract steakpositions from categorystring into an array
        # for example "110->01, 101->02, 001->2, 111->012
        declare -a steakpositions
        local i=0
        while [ $i -lt ${#category} ]; do
            if [ ${category:i:1} -ne 0 ]; then
               steakpositions[${#steakpositions[@]}]=$i
            fi
            i=$((i+1))
        done

        # choose randomly from one of the steakpositions
        local position=0;
        if [ ${#steakpositions[@]} -gt 1 ]; then
            position=$((RANDOM%${#steakpositions[@]}))
        fi

        # change character at chosen steakposition to "1"
        # +1 because sed uses 1 based index
        category="$(printf "000" | sed s/./1/$((${steakpositions[$position]}+1)))"
        printf $category
    }



    # Builds the foldername for specified category
    # Takes global variables for foldernames
    #
    # Arguments:
    #   $1:   Category-string
    #
    # Example:
    #   buildCategoryFolderName 100 => sfw
    #   buildCategoryFolderName 110 => sfw_sketchy
    #   buildCategoryFolderName 101 => sfw_nsfw
    #   buildCategoryFolderName 111 => sfw_sketchy_nsfw
    #
    function buildCategoryFolderName {
        local category=$1
        local sfw=${category:0:1}
        local scetchy=${category:1:1}
        local nsfw=${category:2:1}
        local foldername=""

        if [ $sfw -ne 0 ]; then
            foldername="$SFW_FOLDERNAME"
            if [ $scetchy -ne 0 ]; then
                foldername="$foldername""_"
            fi
        fi
        if [ $scetchy -ne 0 ]; then
            foldername="$foldername$SCETCHY_FOLDERNAME"
            if [ $nsfw -ne 0 ]; then
                foldername="$foldername""_"
            fi
        fi
        if [ $nsfw -ne 0 ]; then
            foldername="$foldername$NSFW_FOLDERNAME"
        fi

        printf $foldername
    }



    # Urlencodes strings for use in internet URLs
    # Parameters:
    #   $@:   String to encode
    # Example:
    #   shellUtils.urlencode "a+b (asd)__23"
    #     ->  a%2Bb%20%28asd%29__%32%33
    # Original source:
    #   http://blogs.gnome.org/shaunm/2009/12/05/urlencode-and-urldecode-in-sh/
    function urlencode {
        # This is important to make sure string manipulation is handled
        # byte-by-byte.
        LANG=C
        local string="$@"
        local i=0
        local encoded=""

        # iterate over each letter in string
        while [ $i -lt ${#string} ]; do
            local c=${string:$i:1}

            if printf "%s" "$c" | grep -q '[a-zA-Z/:_\.\-]'; then
                # no encoding needed
                encoded="$encoded$c"
            else
                # use hexadecimal value for special characters
                local hex=$(printf %X "'$c'")
                encoded="$encoded%$hex"
            fi

            i=$((i+1))
        done

        printf "%s" "$encoded"
    } #urlencode




    # Converts an already encoded string to normal, readyble, text
    # Parameters:
    #   $1:   String to decode
    # Example:
    #   shellUtils.urldecode " a%2Bb%20%28asd%29__%32%33    "
    #     ->  a+b (asd)__23
    # Original source:
    #   http://blogs.gnome.org/shaunm/2009/12/05/urlencode-and-urldecode-in-sh/
    function urldecode {
        local string="$@"
        local i=0
        local decoded=""

        # iterate over each letter in string
        while [ $i -lt ${#string} ]; do
            local c=${string:$i:1}

            # start of hexcode found
            if [ "$c" = "%" ]; then

                # convert hex back to string
                local hex=${string:$((i+1)):2}
                str=$(printf "\x$hex")
                decoded="$decoded$str"

                i=$((i+3))
            else
                # no converting needed
                decoded="$decoded$c"
                i=$((i+1))
            fi
        done

        printf "%s\n" "$decoded"
    } #urldecode




    # logs in to the wallbase website to give the user more functionality
    # requires 2 arguments:
    # arg1: username
    # arg2: password
    #
    function login {
        local user="$1"
        local password="$2"

        # checking parameters -> if not ok print error and exit script
        if [  -z $user ] || [ -z $password ]; then
            printf "No Username and Password specified\n"
            printf "can not continue login process\n"
            printf "Please check USER and PASSWRD fields\n"
            exit 1
        fi

        # everythings ok -> construct login query
        printf "usrname=%s" "$user"      >  login
        printf "&pass=%s"   "$password"  >> login
        printf "&nopass_email=Type+in+your+e-mail+and+press+enter" >> login
        printf "&nopass=0"               >> login
        printf "&1=1\n"                  >> login

        # login and save data in cookie
        wget --keep-session-cookies \
             --save-cookies=cookies.txt \
             --referer=http://wallbase.cc/start/ \
             --post-file=login \
             http://wallbase.cc/user/login >> "$LOGFILE"

        # Get login confirmation and save it in cookie
        wget --keep-session-cookies \
             --load-cookies=cookies.txt \
             --save-cookies=cookies.txt \
             --referer=wallbase.cc \
             http://wallbase.cc/user/adult_confirm/1 >> "$LOGFILE"

        rm login.?

    } # /login




    #
    # downloads Page with Thumbnails
    #
    function getPage {
        local url_params="$1"

        # checking parameters -> if not ok print error and exit script
        if [ -z "$url_params" ]; then
            printf "getPage expects at least 1 argument\n"
            printf "arg1:    parameters for the wget command\n"
            exit 1
        fi

        # parameters ok --> get page
        wget --keep-session-cookies \
             --load-cookies=cookies.txt \
             --referer=wallbase.cc \
             http://wallbase.cc/$url_params >> "$LOGFILE"

    } #getPage




    # Searches wallpaper and sets it to the specified file
    # Parameters:
    #  $1: wallpapernumber
    function setWallpaper {
        number=$1
        if [ -z $number ]; then
            printf "Wrong number of arguments\n"
            printf "Expecting 1 argument [Wallpapernumber]\n"
            exit 1
        fi

        # find wallpaper and save it to specified file
        find "$LOCATION" -path "$LOCATION/*$number*.*" -exec cp {} "$WALLPAPER_FILE" \;

        # mark that the wallpaper has been set
        printf "Wallpaper %s updated\n" "$WALLPAPER_FILE"
        WALLPAPER_SET=1
    } #setWallpaper



    # Arguments:
    #  $1:   parameters for wget command
    function downloadWallpapers {
        # checking parameters -> if not ok print error and exit script
        if [ $# -lt 1 ]; then
            printf "downloadWallpapers expects at least 1 argument\n"
            printf "arg1:    parameters for the wget command\n\n"
            exit 1
        fi

        # parameters ok --> get page
        local pagename=$1
        local URLSFORIMAGES="$(cat $pagename | grep -o "http:.*" | cut -d " " -f 1 | grep wallpaper)"

        for imgURL in $URLSFORIMAGES; do
            local img="$(printf %s    $imgURL | sed 's/.\{1\}$//')"
            local number="$(printf %s $img    | sed 's .\{29\}  ')"

            printf "Wallpaper %-8i ......" "$number"

            # has wallpaper already been downloaded?
            if cat "$WALLPAPER_LOGFILE" | grep "$number" > /dev/null; then
                printf " <3"
                # reuse this one
                if [ $RECYCLE_WALLPAPER -gt 0 ]; then
                    printf " Recycling"
                    setWallpaper $number
                    break;
                fi
                printf "\n"

            else
                printf "</3\n\n"

                # download this wallpaper
                printf "%i" "$number" >> "$WALLPAPER_LOGFILE"
                wget --keep-session-cookies --load-cookies=cookies.txt --referer=wallbase.cc $img
                cat $number | grep -o "'+B.*+" | sed 's/.\{3\}$//' | sed 's .\{5\}  ' | base64 -d | wget --keep-session-cookies --load-cookies=cookies.txt --referer=http://wallbase.cc/wallpaper/$number -i -
                rm -f $number

               # download only one new wallpaper and replace desktop-wallpaper
                if [ $AUTOSET -gt 0 ]; then
                    setWallpaper $number
                    break;
                fi

            fi #[ cat downloaded.txt | grep "$number" >/dev/null ]
        done # ( imgURL in $URLSFORIMAGES )
        rm $pagename
    } #/downloadWallpapers




###### -------------ARGUMENTS ---------------
###### --------------------------------------
    # IF SEARCH QUERY IS AVAILABLE
    if [ $# -gt 0 ]; then
        QUERY="$@"
    fi
    QUERY=$(urlencode "$QUERY")



###### ----------MAIN PROGRAM ---------------
###### --------------------------------------

    # categorize the downloads by their
    # CATEGORY(nsfw,sfw,sketchy)
    # and TOPIC (manga, hd, general)
    CATEGORY="$(fetchCategory $SFW $SCETCHY $NSFW)"
    CATEGORY_FOLDER="$(buildCategoryFolderName $CATEGORY)"
    if [ $CATEGORIZE -gt 0 ]; then
        LOCATION="$LOCATION/$CATEGORY_FOLDER"
        if [ $TYPE -eq 4 ]; then
            LOCATION="$LOCATION/$(urldecode $QUERY)"
        fi
        LOCATION="$LOCATION/$TOPIC"
    fi

    mkdir -p "$LOCATION"
    cd "$LOCATION"


    # login only when needed
    # NSFW content, favourites or user collections
    if [ $NSFW -ne 0 ] || [ $TYPE -eq 5 ] || [ $TYPE -eq 6 ]; then
        login $USER $PASS
    fi


    case $TYPE in
        1) # RANDOM
            for (( count= 0; count< "$MAX_RANGE"; count=count+$THPP ));
            do
                getPage random/$TOPIC/$SIZE/$RESOLUTION/$ASPECTRATIO/$CATEGORY/$THPP
                downloadWallpapers $THPP

                if [ $WALLPAPER_SET -eq 1 ]; then
                    # wallpaper downloaded and set, no need to download more
                    break
                fi
            done
        ;;
        2) # TOPLIST
            for (( count= 0; count< "$MAX_RANGE"; count=count+$THPP ));
            do
                getPage toplist/$count/$TOPIC/$SIZE/$RESOLUTION/$ASPECTRATIO/$CATEGORY/$THPP/$TIME
                downloadWallpapers $TIME

                if [ $WALLPAPER_SET -eq 1 ]; then
                    # wallpaper downloaded and set, no need to download more
                    break
                fi
            done
        ;;
        3) # NEWEST
            for (( count= 0; count< "$MAX_RANGE"; count=count+"$THPP" ));
            do
                getPage search/$count/$TOPIC/$SIZE/$RESOLUTION/$ASPECTRATIO/$CATEGORY/$THPP
                downloadWallpapers $THPP

                if [ $WALLPAPER_SET -eq 1 ]; then
                    # wallpaper downloaded and set, no need to download more
                    break
                fi
            done
        ;;
        4) # SEARCH
            # construct propper url for search-query
            printf "query=%s"      "$QUERY"          > data
            printf "&board=%s"    "$TOPIC"          >> data
            printf "&res_opt=%s"  "$SIZE"           >> data
            printf "&res=%s"      "$RESOLUTION"     >> data
            printf "&aspect=%s"   "$ASPECTRATIO"    >> data
            printf "&nsfw_sfw=%s"     "${CATEGORY:0:1}" >> data
            printf "&nsfw_sketchy=%s" "${CATEGORY:1:1}"  >> data
            printf "&nsfw_nsfw=%s"    "${CATEGORY:2:1}"  >> data
            printf "&thpp=%s"        "$THPP"          >> data
            printf "&orderby=%s"     "$ORDER"         >> data
            printf "&orderby_opt=%s" "$ORDER_TYPE"    >> data
            printf "&section=wallpapers"              >> data
            printf "&1=1\n"                           >> data

            for (( count= $THPP; count < "$MAX_RANGE"; count=count+$THPP )); do
                wget --keep-session-cookies \
                     --load-cookies="cookies.txt" \
                     --referer="wallbase.cc/search" \
                     --post-file="data" "http://wallbase.cc/search/$count" > "$LOGFILE"
                downloadWallpapers $count

                if [ $WALLPAPER_SET -eq 1 ]; then
                    # wallpaper downloaded and set, no need to download more
                    break
                fi
            done
            rm data
        ;;

        5) # FAVOURITES
            for (( count= 0; count < "$MAX_RANGE"; count=count+32 )); do
                getPage user/favorites/$COLLECTION/$count
                downloadWallpapers $count

                if [ $WALLPAPER_SET -eq 1 ]; then
                    # wallpaper downloaded and set, no need to download more
                    break
                fi
            done
        ;;
        6) # USER CREATED COLLECTIONS
            for (( count= 0; count < "$MAX_RANGE"; count=count+32 )); do
                getPage user/collection/$COLLECTION/1/$count
                downloadWallpapers $count

                if [ $WALLPAPER_SET -eq 1 ]; then
                    # wallpaper downloaded and set, no need to download more
                    break
                fi
            done
        ;;
        7) # UPLOADS FROM SPECIFIC USER
            for (( count= 0; count < "$MAX_RANGE"; count=count+32 )); do
                getPage user/profile/$COLLECTION/uploads/date/0/$count
                downloadWallpapers $count

                if [ $WALLPAPER_SET -eq 1 ]; then
                    # wallpaper downloaded and set, no need to download more
                    break
                fi
            done
        ;;
        *)
            printf "error in TYPE please check Variable\n"
        ;;
    esac

    rm "1" "cookies.txt" "login"


